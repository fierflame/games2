const game = 'demine';
const serviceRoot = `/${game}/`;
const cacheVersion = '1646027690602';

const files = new Set(['index.js', 'index.html']);

async function backup(path) {
  const res = await fetch(`./${path}?_=${Number(new Date())}`);

  if (res.status !== 200) {
    return res;
  }

  const cache = await caches.open(`${game}/${cacheVersion}`);
  cache.put(`./${path}`, res.clone());
  return res;
}
async function backupAll() {
  await Promise.all([...files].map(backup));
}

async function clearCache(cacheName) {
  const [n, v] = cacheName.split('/');

  if (n !== game) {
    return;
  }

  if (v === cacheVersion) {
    return;
  }

  await caches.delete(cacheName);
}

async function clearCaches() {
  const keys = await caches.keys();
  await Promise.all(keys.map(clearCache));
}

const regex = /^(\/\d+){3}$/;
function transPath(p) {
  if (regex.test(p)) {
    return '/';
  }

  return p;
}

function getSubPath(url) {
  const {
    pathname
  } = new URL(url);

  if (pathname.substring(0, serviceRoot.length) !== serviceRoot) {
    return '';
  }

  const path = pathname.substring(serviceRoot.length - 1).replace(/[/\\]+/g, '/');
  return path;
}

function getPath(url) {
  let path = getSubPath(url);

  if (!path) {
    return '';
  }

  path = transPath(path) || path;
  let paths = path.split('/').filter(Boolean);

  if (path[path.length - 1] === '/') {
    paths.push('index.html');
  }

  const filepath = paths.join('/');

  if (files.has(filepath)) {
    return filepath;
  }

  return '';
}

self.addEventListener('fetch', function (evt) {
  const event = evt;

  if (event.request.method !== 'GET') {
    return;
  }

  const path = getPath(event.request.url);

  if (!path) {
    return;
  }

  if (typeof path !== 'string') {
    return;
  }

  event.respondWith(caches.match(path).then(res => res || backup(path)));
});
self.addEventListener('install', function (event) {
  event.waitUntil(backupAll());
});
self.addEventListener('activate', function (event) {
  event.waitUntil(Promise.all([self.clients.claim(), clearCaches()]));
});
