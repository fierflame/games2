function __$styleInject(css) {
    if (!css) return;

    if (typeof window == 'undefined') return;
    var style = document.createElement('style');
    style.setAttribute('media', 'screen');

    style.innerHTML = css;
    document.head.appendChild(style);
    return css;
}

__$styleInject(".wrong {\n  position: relative;\n  background-color: #f00;\n}\n.wrong::before {\n  content: '💥';\n}\n.mine::before {\n  content: '💣';\n}\n.known {\n  background-color: #0f0;\n}\n.flag::before {\n  content: '🚩';\n}\n.flagNoMine::before {\n  content: '🏴';\n}\n.demine-area {\n  display: grid;\n  border: 1px solid;\n  box-sizing: border-box;\n}\n.demine-area-row-1 {\n  grid-template-rows: repeat(1, 20px);\n  block-size: 22px;\n}\n.demine-area-row-2 {\n  grid-template-rows: repeat(2, 20px);\n  block-size: 42px;\n}\n.demine-area-row-3 {\n  grid-template-rows: repeat(3, 20px);\n  block-size: 62px;\n}\n.demine-area-row-4 {\n  grid-template-rows: repeat(4, 20px);\n  block-size: 82px;\n}\n.demine-area-row-5 {\n  grid-template-rows: repeat(5, 20px);\n  block-size: 102px;\n}\n.demine-area-row-6 {\n  grid-template-rows: repeat(6, 20px);\n  block-size: 122px;\n}\n.demine-area-row-7 {\n  grid-template-rows: repeat(7, 20px);\n  block-size: 142px;\n}\n.demine-area-row-8 {\n  grid-template-rows: repeat(8, 20px);\n  block-size: 162px;\n}\n.demine-area-row-9 {\n  grid-template-rows: repeat(9, 20px);\n  block-size: 182px;\n}\n.demine-area-row-10 {\n  grid-template-rows: repeat(10, 20px);\n  block-size: 202px;\n}\n.demine-area-row-11 {\n  grid-template-rows: repeat(11, 20px);\n  block-size: 222px;\n}\n.demine-area-row-12 {\n  grid-template-rows: repeat(12, 20px);\n  block-size: 242px;\n}\n.demine-area-row-13 {\n  grid-template-rows: repeat(13, 20px);\n  block-size: 262px;\n}\n.demine-area-row-14 {\n  grid-template-rows: repeat(14, 20px);\n  block-size: 282px;\n}\n.demine-area-row-15 {\n  grid-template-rows: repeat(15, 20px);\n  block-size: 302px;\n}\n.demine-area-row-16 {\n  grid-template-rows: repeat(16, 20px);\n  block-size: 322px;\n}\n.demine-area-row-17 {\n  grid-template-rows: repeat(17, 20px);\n  block-size: 342px;\n}\n.demine-area-row-18 {\n  grid-template-rows: repeat(18, 20px);\n  block-size: 362px;\n}\n.demine-area-row-19 {\n  grid-template-rows: repeat(19, 20px);\n  block-size: 382px;\n}\n.demine-area-row-20 {\n  grid-template-rows: repeat(20, 20px);\n  block-size: 402px;\n}\n.demine-area-row-21 {\n  grid-template-rows: repeat(21, 20px);\n  block-size: 422px;\n}\n.demine-area-row-22 {\n  grid-template-rows: repeat(22, 20px);\n  block-size: 442px;\n}\n.demine-area-row-23 {\n  grid-template-rows: repeat(23, 20px);\n  block-size: 462px;\n}\n.demine-area-row-24 {\n  grid-template-rows: repeat(24, 20px);\n  block-size: 482px;\n}\n.demine-area-row-25 {\n  grid-template-rows: repeat(25, 20px);\n  block-size: 502px;\n}\n.demine-area-row-26 {\n  grid-template-rows: repeat(26, 20px);\n  block-size: 522px;\n}\n.demine-area-row-27 {\n  grid-template-rows: repeat(27, 20px);\n  block-size: 542px;\n}\n.demine-area-row-28 {\n  grid-template-rows: repeat(28, 20px);\n  block-size: 562px;\n}\n.demine-area-row-29 {\n  grid-template-rows: repeat(29, 20px);\n  block-size: 582px;\n}\n.demine-area-row-30 {\n  grid-template-rows: repeat(30, 20px);\n  block-size: 602px;\n}\n.demine-area-row-31 {\n  grid-template-rows: repeat(31, 20px);\n  block-size: 622px;\n}\n.demine-area-row-32 {\n  grid-template-rows: repeat(32, 20px);\n  block-size: 642px;\n}\n.demine-area-row-33 {\n  grid-template-rows: repeat(33, 20px);\n  block-size: 662px;\n}\n.demine-area-row-34 {\n  grid-template-rows: repeat(34, 20px);\n  block-size: 682px;\n}\n.demine-area-row-35 {\n  grid-template-rows: repeat(35, 20px);\n  block-size: 702px;\n}\n.demine-area-row-36 {\n  grid-template-rows: repeat(36, 20px);\n  block-size: 722px;\n}\n.demine-area-row-37 {\n  grid-template-rows: repeat(37, 20px);\n  block-size: 742px;\n}\n.demine-area-row-38 {\n  grid-template-rows: repeat(38, 20px);\n  block-size: 762px;\n}\n.demine-area-row-39 {\n  grid-template-rows: repeat(39, 20px);\n  block-size: 782px;\n}\n.demine-area-row-40 {\n  grid-template-rows: repeat(40, 20px);\n  block-size: 802px;\n}\n.demine-area-row-41 {\n  grid-template-rows: repeat(41, 20px);\n  block-size: 822px;\n}\n.demine-area-row-42 {\n  grid-template-rows: repeat(42, 20px);\n  block-size: 842px;\n}\n.demine-area-row-43 {\n  grid-template-rows: repeat(43, 20px);\n  block-size: 862px;\n}\n.demine-area-row-44 {\n  grid-template-rows: repeat(44, 20px);\n  block-size: 882px;\n}\n.demine-area-row-45 {\n  grid-template-rows: repeat(45, 20px);\n  block-size: 902px;\n}\n.demine-area-row-46 {\n  grid-template-rows: repeat(46, 20px);\n  block-size: 922px;\n}\n.demine-area-row-47 {\n  grid-template-rows: repeat(47, 20px);\n  block-size: 942px;\n}\n.demine-area-row-48 {\n  grid-template-rows: repeat(48, 20px);\n  block-size: 962px;\n}\n.demine-area-row-49 {\n  grid-template-rows: repeat(49, 20px);\n  block-size: 982px;\n}\n.demine-area-row-50 {\n  grid-template-rows: repeat(50, 20px);\n  block-size: 1002px;\n}\n.demine-area-row-51 {\n  grid-template-rows: repeat(51, 20px);\n  block-size: 1022px;\n}\n.demine-area-row-52 {\n  grid-template-rows: repeat(52, 20px);\n  block-size: 1042px;\n}\n.demine-area-row-53 {\n  grid-template-rows: repeat(53, 20px);\n  block-size: 1062px;\n}\n.demine-area-row-54 {\n  grid-template-rows: repeat(54, 20px);\n  block-size: 1082px;\n}\n.demine-area-row-55 {\n  grid-template-rows: repeat(55, 20px);\n  block-size: 1102px;\n}\n.demine-area-row-56 {\n  grid-template-rows: repeat(56, 20px);\n  block-size: 1122px;\n}\n.demine-area-row-57 {\n  grid-template-rows: repeat(57, 20px);\n  block-size: 1142px;\n}\n.demine-area-row-58 {\n  grid-template-rows: repeat(58, 20px);\n  block-size: 1162px;\n}\n.demine-area-row-59 {\n  grid-template-rows: repeat(59, 20px);\n  block-size: 1182px;\n}\n.demine-area-row-60 {\n  grid-template-rows: repeat(60, 20px);\n  block-size: 1202px;\n}\n.demine-area-row-61 {\n  grid-template-rows: repeat(61, 20px);\n  block-size: 1222px;\n}\n.demine-area-row-62 {\n  grid-template-rows: repeat(62, 20px);\n  block-size: 1242px;\n}\n.demine-area-row-63 {\n  grid-template-rows: repeat(63, 20px);\n  block-size: 1262px;\n}\n.demine-area-row-64 {\n  grid-template-rows: repeat(64, 20px);\n  block-size: 1282px;\n}\n.demine-area-row-65 {\n  grid-template-rows: repeat(65, 20px);\n  block-size: 1302px;\n}\n.demine-area-row-66 {\n  grid-template-rows: repeat(66, 20px);\n  block-size: 1322px;\n}\n.demine-area-row-67 {\n  grid-template-rows: repeat(67, 20px);\n  block-size: 1342px;\n}\n.demine-area-row-68 {\n  grid-template-rows: repeat(68, 20px);\n  block-size: 1362px;\n}\n.demine-area-row-69 {\n  grid-template-rows: repeat(69, 20px);\n  block-size: 1382px;\n}\n.demine-area-row-70 {\n  grid-template-rows: repeat(70, 20px);\n  block-size: 1402px;\n}\n.demine-area-row-71 {\n  grid-template-rows: repeat(71, 20px);\n  block-size: 1422px;\n}\n.demine-area-row-72 {\n  grid-template-rows: repeat(72, 20px);\n  block-size: 1442px;\n}\n.demine-area-row-73 {\n  grid-template-rows: repeat(73, 20px);\n  block-size: 1462px;\n}\n.demine-area-row-74 {\n  grid-template-rows: repeat(74, 20px);\n  block-size: 1482px;\n}\n.demine-area-row-75 {\n  grid-template-rows: repeat(75, 20px);\n  block-size: 1502px;\n}\n.demine-area-row-76 {\n  grid-template-rows: repeat(76, 20px);\n  block-size: 1522px;\n}\n.demine-area-row-77 {\n  grid-template-rows: repeat(77, 20px);\n  block-size: 1542px;\n}\n.demine-area-row-78 {\n  grid-template-rows: repeat(78, 20px);\n  block-size: 1562px;\n}\n.demine-area-row-79 {\n  grid-template-rows: repeat(79, 20px);\n  block-size: 1582px;\n}\n.demine-area-row-80 {\n  grid-template-rows: repeat(80, 20px);\n  block-size: 1602px;\n}\n.demine-area-row-81 {\n  grid-template-rows: repeat(81, 20px);\n  block-size: 1622px;\n}\n.demine-area-row-82 {\n  grid-template-rows: repeat(82, 20px);\n  block-size: 1642px;\n}\n.demine-area-row-83 {\n  grid-template-rows: repeat(83, 20px);\n  block-size: 1662px;\n}\n.demine-area-row-84 {\n  grid-template-rows: repeat(84, 20px);\n  block-size: 1682px;\n}\n.demine-area-row-85 {\n  grid-template-rows: repeat(85, 20px);\n  block-size: 1702px;\n}\n.demine-area-row-86 {\n  grid-template-rows: repeat(86, 20px);\n  block-size: 1722px;\n}\n.demine-area-row-87 {\n  grid-template-rows: repeat(87, 20px);\n  block-size: 1742px;\n}\n.demine-area-row-88 {\n  grid-template-rows: repeat(88, 20px);\n  block-size: 1762px;\n}\n.demine-area-row-89 {\n  grid-template-rows: repeat(89, 20px);\n  block-size: 1782px;\n}\n.demine-area-row-90 {\n  grid-template-rows: repeat(90, 20px);\n  block-size: 1802px;\n}\n.demine-area-row-91 {\n  grid-template-rows: repeat(91, 20px);\n  block-size: 1822px;\n}\n.demine-area-row-92 {\n  grid-template-rows: repeat(92, 20px);\n  block-size: 1842px;\n}\n.demine-area-row-93 {\n  grid-template-rows: repeat(93, 20px);\n  block-size: 1862px;\n}\n.demine-area-row-94 {\n  grid-template-rows: repeat(94, 20px);\n  block-size: 1882px;\n}\n.demine-area-row-95 {\n  grid-template-rows: repeat(95, 20px);\n  block-size: 1902px;\n}\n.demine-area-row-96 {\n  grid-template-rows: repeat(96, 20px);\n  block-size: 1922px;\n}\n.demine-area-row-97 {\n  grid-template-rows: repeat(97, 20px);\n  block-size: 1942px;\n}\n.demine-area-row-98 {\n  grid-template-rows: repeat(98, 20px);\n  block-size: 1962px;\n}\n.demine-area-row-99 {\n  grid-template-rows: repeat(99, 20px);\n  block-size: 1982px;\n}\n.demine-area-row-100 {\n  grid-template-rows: repeat(100, 20px);\n  block-size: 2002px;\n}\n.demine-area-column-1 {\n  grid-template-columns: repeat(1, 20px);\n  inline-size: 22px;\n}\n.demine-area-column-2 {\n  grid-template-columns: repeat(2, 20px);\n  inline-size: 42px;\n}\n.demine-area-column-3 {\n  grid-template-columns: repeat(3, 20px);\n  inline-size: 62px;\n}\n.demine-area-column-4 {\n  grid-template-columns: repeat(4, 20px);\n  inline-size: 82px;\n}\n.demine-area-column-5 {\n  grid-template-columns: repeat(5, 20px);\n  inline-size: 102px;\n}\n.demine-area-column-6 {\n  grid-template-columns: repeat(6, 20px);\n  inline-size: 122px;\n}\n.demine-area-column-7 {\n  grid-template-columns: repeat(7, 20px);\n  inline-size: 142px;\n}\n.demine-area-column-8 {\n  grid-template-columns: repeat(8, 20px);\n  inline-size: 162px;\n}\n.demine-area-column-9 {\n  grid-template-columns: repeat(9, 20px);\n  inline-size: 182px;\n}\n.demine-area-column-10 {\n  grid-template-columns: repeat(10, 20px);\n  inline-size: 202px;\n}\n.demine-area-column-11 {\n  grid-template-columns: repeat(11, 20px);\n  inline-size: 222px;\n}\n.demine-area-column-12 {\n  grid-template-columns: repeat(12, 20px);\n  inline-size: 242px;\n}\n.demine-area-column-13 {\n  grid-template-columns: repeat(13, 20px);\n  inline-size: 262px;\n}\n.demine-area-column-14 {\n  grid-template-columns: repeat(14, 20px);\n  inline-size: 282px;\n}\n.demine-area-column-15 {\n  grid-template-columns: repeat(15, 20px);\n  inline-size: 302px;\n}\n.demine-area-column-16 {\n  grid-template-columns: repeat(16, 20px);\n  inline-size: 322px;\n}\n.demine-area-column-17 {\n  grid-template-columns: repeat(17, 20px);\n  inline-size: 342px;\n}\n.demine-area-column-18 {\n  grid-template-columns: repeat(18, 20px);\n  inline-size: 362px;\n}\n.demine-area-column-19 {\n  grid-template-columns: repeat(19, 20px);\n  inline-size: 382px;\n}\n.demine-area-column-20 {\n  grid-template-columns: repeat(20, 20px);\n  inline-size: 402px;\n}\n.demine-area-column-21 {\n  grid-template-columns: repeat(21, 20px);\n  inline-size: 422px;\n}\n.demine-area-column-22 {\n  grid-template-columns: repeat(22, 20px);\n  inline-size: 442px;\n}\n.demine-area-column-23 {\n  grid-template-columns: repeat(23, 20px);\n  inline-size: 462px;\n}\n.demine-area-column-24 {\n  grid-template-columns: repeat(24, 20px);\n  inline-size: 482px;\n}\n.demine-area-column-25 {\n  grid-template-columns: repeat(25, 20px);\n  inline-size: 502px;\n}\n.demine-area-column-26 {\n  grid-template-columns: repeat(26, 20px);\n  inline-size: 522px;\n}\n.demine-area-column-27 {\n  grid-template-columns: repeat(27, 20px);\n  inline-size: 542px;\n}\n.demine-area-column-28 {\n  grid-template-columns: repeat(28, 20px);\n  inline-size: 562px;\n}\n.demine-area-column-29 {\n  grid-template-columns: repeat(29, 20px);\n  inline-size: 582px;\n}\n.demine-area-column-30 {\n  grid-template-columns: repeat(30, 20px);\n  inline-size: 602px;\n}\n.demine-area-column-31 {\n  grid-template-columns: repeat(31, 20px);\n  inline-size: 622px;\n}\n.demine-area-column-32 {\n  grid-template-columns: repeat(32, 20px);\n  inline-size: 642px;\n}\n.demine-area-column-33 {\n  grid-template-columns: repeat(33, 20px);\n  inline-size: 662px;\n}\n.demine-area-column-34 {\n  grid-template-columns: repeat(34, 20px);\n  inline-size: 682px;\n}\n.demine-area-column-35 {\n  grid-template-columns: repeat(35, 20px);\n  inline-size: 702px;\n}\n.demine-area-column-36 {\n  grid-template-columns: repeat(36, 20px);\n  inline-size: 722px;\n}\n.demine-area-column-37 {\n  grid-template-columns: repeat(37, 20px);\n  inline-size: 742px;\n}\n.demine-area-column-38 {\n  grid-template-columns: repeat(38, 20px);\n  inline-size: 762px;\n}\n.demine-area-column-39 {\n  grid-template-columns: repeat(39, 20px);\n  inline-size: 782px;\n}\n.demine-area-column-40 {\n  grid-template-columns: repeat(40, 20px);\n  inline-size: 802px;\n}\n.demine-area-column-41 {\n  grid-template-columns: repeat(41, 20px);\n  inline-size: 822px;\n}\n.demine-area-column-42 {\n  grid-template-columns: repeat(42, 20px);\n  inline-size: 842px;\n}\n.demine-area-column-43 {\n  grid-template-columns: repeat(43, 20px);\n  inline-size: 862px;\n}\n.demine-area-column-44 {\n  grid-template-columns: repeat(44, 20px);\n  inline-size: 882px;\n}\n.demine-area-column-45 {\n  grid-template-columns: repeat(45, 20px);\n  inline-size: 902px;\n}\n.demine-area-column-46 {\n  grid-template-columns: repeat(46, 20px);\n  inline-size: 922px;\n}\n.demine-area-column-47 {\n  grid-template-columns: repeat(47, 20px);\n  inline-size: 942px;\n}\n.demine-area-column-48 {\n  grid-template-columns: repeat(48, 20px);\n  inline-size: 962px;\n}\n.demine-area-column-49 {\n  grid-template-columns: repeat(49, 20px);\n  inline-size: 982px;\n}\n.demine-area-column-50 {\n  grid-template-columns: repeat(50, 20px);\n  inline-size: 1002px;\n}\n.demine-area-column-51 {\n  grid-template-columns: repeat(51, 20px);\n  inline-size: 1022px;\n}\n.demine-area-column-52 {\n  grid-template-columns: repeat(52, 20px);\n  inline-size: 1042px;\n}\n.demine-area-column-53 {\n  grid-template-columns: repeat(53, 20px);\n  inline-size: 1062px;\n}\n.demine-area-column-54 {\n  grid-template-columns: repeat(54, 20px);\n  inline-size: 1082px;\n}\n.demine-area-column-55 {\n  grid-template-columns: repeat(55, 20px);\n  inline-size: 1102px;\n}\n.demine-area-column-56 {\n  grid-template-columns: repeat(56, 20px);\n  inline-size: 1122px;\n}\n.demine-area-column-57 {\n  grid-template-columns: repeat(57, 20px);\n  inline-size: 1142px;\n}\n.demine-area-column-58 {\n  grid-template-columns: repeat(58, 20px);\n  inline-size: 1162px;\n}\n.demine-area-column-59 {\n  grid-template-columns: repeat(59, 20px);\n  inline-size: 1182px;\n}\n.demine-area-column-60 {\n  grid-template-columns: repeat(60, 20px);\n  inline-size: 1202px;\n}\n.demine-area-column-61 {\n  grid-template-columns: repeat(61, 20px);\n  inline-size: 1222px;\n}\n.demine-area-column-62 {\n  grid-template-columns: repeat(62, 20px);\n  inline-size: 1242px;\n}\n.demine-area-column-63 {\n  grid-template-columns: repeat(63, 20px);\n  inline-size: 1262px;\n}\n.demine-area-column-64 {\n  grid-template-columns: repeat(64, 20px);\n  inline-size: 1282px;\n}\n.demine-area-column-65 {\n  grid-template-columns: repeat(65, 20px);\n  inline-size: 1302px;\n}\n.demine-area-column-66 {\n  grid-template-columns: repeat(66, 20px);\n  inline-size: 1322px;\n}\n.demine-area-column-67 {\n  grid-template-columns: repeat(67, 20px);\n  inline-size: 1342px;\n}\n.demine-area-column-68 {\n  grid-template-columns: repeat(68, 20px);\n  inline-size: 1362px;\n}\n.demine-area-column-69 {\n  grid-template-columns: repeat(69, 20px);\n  inline-size: 1382px;\n}\n.demine-area-column-70 {\n  grid-template-columns: repeat(70, 20px);\n  inline-size: 1402px;\n}\n.demine-area-column-71 {\n  grid-template-columns: repeat(71, 20px);\n  inline-size: 1422px;\n}\n.demine-area-column-72 {\n  grid-template-columns: repeat(72, 20px);\n  inline-size: 1442px;\n}\n.demine-area-column-73 {\n  grid-template-columns: repeat(73, 20px);\n  inline-size: 1462px;\n}\n.demine-area-column-74 {\n  grid-template-columns: repeat(74, 20px);\n  inline-size: 1482px;\n}\n.demine-area-column-75 {\n  grid-template-columns: repeat(75, 20px);\n  inline-size: 1502px;\n}\n.demine-area-column-76 {\n  grid-template-columns: repeat(76, 20px);\n  inline-size: 1522px;\n}\n.demine-area-column-77 {\n  grid-template-columns: repeat(77, 20px);\n  inline-size: 1542px;\n}\n.demine-area-column-78 {\n  grid-template-columns: repeat(78, 20px);\n  inline-size: 1562px;\n}\n.demine-area-column-79 {\n  grid-template-columns: repeat(79, 20px);\n  inline-size: 1582px;\n}\n.demine-area-column-80 {\n  grid-template-columns: repeat(80, 20px);\n  inline-size: 1602px;\n}\n.demine-area-column-81 {\n  grid-template-columns: repeat(81, 20px);\n  inline-size: 1622px;\n}\n.demine-area-column-82 {\n  grid-template-columns: repeat(82, 20px);\n  inline-size: 1642px;\n}\n.demine-area-column-83 {\n  grid-template-columns: repeat(83, 20px);\n  inline-size: 1662px;\n}\n.demine-area-column-84 {\n  grid-template-columns: repeat(84, 20px);\n  inline-size: 1682px;\n}\n.demine-area-column-85 {\n  grid-template-columns: repeat(85, 20px);\n  inline-size: 1702px;\n}\n.demine-area-column-86 {\n  grid-template-columns: repeat(86, 20px);\n  inline-size: 1722px;\n}\n.demine-area-column-87 {\n  grid-template-columns: repeat(87, 20px);\n  inline-size: 1742px;\n}\n.demine-area-column-88 {\n  grid-template-columns: repeat(88, 20px);\n  inline-size: 1762px;\n}\n.demine-area-column-89 {\n  grid-template-columns: repeat(89, 20px);\n  inline-size: 1782px;\n}\n.demine-area-column-90 {\n  grid-template-columns: repeat(90, 20px);\n  inline-size: 1802px;\n}\n.demine-area-column-91 {\n  grid-template-columns: repeat(91, 20px);\n  inline-size: 1822px;\n}\n.demine-area-column-92 {\n  grid-template-columns: repeat(92, 20px);\n  inline-size: 1842px;\n}\n.demine-area-column-93 {\n  grid-template-columns: repeat(93, 20px);\n  inline-size: 1862px;\n}\n.demine-area-column-94 {\n  grid-template-columns: repeat(94, 20px);\n  inline-size: 1882px;\n}\n.demine-area-column-95 {\n  grid-template-columns: repeat(95, 20px);\n  inline-size: 1902px;\n}\n.demine-area-column-96 {\n  grid-template-columns: repeat(96, 20px);\n  inline-size: 1922px;\n}\n.demine-area-column-97 {\n  grid-template-columns: repeat(97, 20px);\n  inline-size: 1942px;\n}\n.demine-area-column-98 {\n  grid-template-columns: repeat(98, 20px);\n  inline-size: 1962px;\n}\n.demine-area-column-99 {\n  grid-template-columns: repeat(99, 20px);\n  inline-size: 1982px;\n}\n.demine-area-column-100 {\n  grid-template-columns: repeat(100, 20px);\n  inline-size: 2002px;\n}\n.demine-area > * {\n  font-size: 10px;\n  line-height: 18px;\n  text-align: center;\n  border: 1px solid;\n}\nhtml,\nbody {\n  padding: 0;\n  margin: 0;\n  user-select: none;\n}\nbody {\n  background: #808080;\n}\n.page {\n  width: 100vw;\n  height: 100vh;\n}\nform {\n  display: flex;\n  flex-direction: column;\n  inline-size: 240px;\n  padding: 40px;\n}\nform > div {\n  display: flex;\n  flex-direction: row;\n}\nform > div * {\n  flex: 1;\n  width: auto;\n  inline-size: auto;\n  text-align: center;\n  min-inline-size: 0;\n}\n.main {\n  display: flex;\n  flex-direction: column;\n}\n.main > header {\n  block-size: 48px;\n  display: flex;\n  flex-direction: row;\n}\n.main > header div {\n  flex: 1;\n  text-align: center;\n  line-height: 48px;\n}\n.main > header button {\n  padding: 0;\n  border: none;\n  margin-block: 0;\n  margin-inline: 12px;\n  text-align: center;\n  line-height: 48px;\n  background: #ccc;\n  border-radius: 100%;\n  width: 48px;\n  box-sizing: border-box;\n  color: #333;\n}\n.main > header button.selected {\n  background: #333;\n  color: #CCC;\n}\n.main > :last-child {\n  flex: 1;\n}\n.main > :last-child > .result {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.8);\n  padding: 10px;\n  inline-size: 240px;\n  block-size: 200px;\n  margin: auto;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  display: flex;\n  flex-direction: column;\n  border-radius: 10px;\n}\n.main > :last-child > .result .close {\n  display: block;\n  top: 4px;\n  right: 4px;\n  position: absolute;\n  padding: 0;\n  border: none;\n  text-align: center;\n  line-height: 24px;\n  background: #ccc;\n  border-radius: 100%;\n  width: 24px;\n  font-size: 24px;\n  box-sizing: border-box;\n  color: #333;\n}\n.main > :last-child > .result .close::after {\n  content: '×';\n}\n.main > :last-child > .result p {\n  margin: 0;\n  padding: 0;\n  color: #ccc;\n  flex: 1;\n  font-size: 16px;\n  line-height: 36px;\n}\n.main > :last-child > .result .title {\n  margin: 0;\n  padding: 0;\n  color: #ccc;\n  text-align: center;\n  font-size: 36px;\n  line-height: 48px;\n}\n.main > :last-child > .result div {\n  display: flex;\n  flex-direction: row;\n}\n.main > :last-child > .result div button {\n  flex: 1;\n  padding: 0;\n  border: none;\n  margin-inline: 4px;\n  text-align: center;\n  line-height: 48px;\n  background: #ccc;\n  box-sizing: border-box;\n  color: #333;\n  border-radius: 10px;\n  font-size: 20px;\n}\n.main > :last-child > .result div button:last-child {\n  color: #ccc;\n  background-color: #6699FF;\n}\n.main > :last-child > .result div button:hover {\n  font-weight: bold;\n}\n");

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function init(el, children) {
  const shadow = el.attachShadow({
    mode: 'closed'
  });
  const style = shadow.appendChild(document.createElement('style'));
  style.textContent = `:host{
		display: block;
		overflow: hidden;
		position: relative;
	}`;
  shadow.appendChild(document.createElement('slot'));
  const observer = new ResizeObserver(e => {
    const last = e.pop();

    if (!last) {
      return;
    }

    const {
      contentRect: {
        width,
        height
      }
    } = last;

    for (const t of children) {
      t.resize(width, height);
    }
  });

  function getTarget(event) {
    const {
      currentTarget
    } = event;

    if (!currentTarget) {
      return null;
    }

    const list = event.composedPath();
    const k = list.findIndex(t => t === currentTarget);
    const e = k < 0 ? [] : list.slice(0, k).reverse();

    for (const child of [...children]) {
      const r = child.match(e);

      if (child.match(e)) {
        return child;
      }

      if (r === null) {
        return null;
      }
    }

    return null;
  }

  const mouseTargets = {};
  const mouseDown = [];

  function updateMouseDown(event, isMove) {
    const {
      buttons,
      pageX,
      pageY,
      currentTarget
    } = event;

    if (!currentTarget) {
      return null;
    }

    const {
      x: OffX,
      y: OffY
    } = currentTarget.getBoundingClientRect();
    const x = pageX - OffX;
    const y = pageY - OffY;
    const target = getTarget(event);

    for (let k = 0; k < 5; k++) {
      const down = Boolean(buttons & 1 << k);

      if (down === Boolean(mouseDown[k])) {
        continue;
      }

      mouseDown[k] = down;

      if (down) {
        if (isMove !== true && target) {
          mouseTargets[k] = target;
          target.mouseBegin(x, y, k);
        }
      } else {
        const t = mouseTargets[k];

        if (t) {
          t.mouseEnd(k);
        }

        delete mouseTargets[k];
      }
    }

    return target;
  }

  function mouseUpAll() {
    for (let k = 0; k < 5; k++) {
      const t = mouseTargets[k];

      if (t) {
        t.mouseEnd(k);
      }

      delete mouseTargets[k];
    }

    for (let k = 0; k < 5; k++) {
      mouseDown[k] = false;
    }
  }

  function mousemove(event) {
    const {
      pageX,
      pageY,
      currentTarget
    } = event;

    if (!currentTarget) {
      return;
    }

    const {
      x: OffX,
      y: OffY
    } = currentTarget.getBoundingClientRect();
    const x = pageX - OffX;
    const y = pageY - OffY;

    for (const t of Object.values(mouseTargets)) {
      t.mouseMove(x, y);
    }
  }

  const touchTargets = {};

  function touchdown(event) {
    const {
      pageX,
      pageY,
      currentTarget
    } = event;

    if (!currentTarget) {
      return;
    }

    const target = getTarget(event);

    if (!target) {
      return;
    }

    const {
      pointerId
    } = event;
    touchTargets[pointerId] = target;
    const {
      x: OffX,
      y: OffY
    } = currentTarget.getBoundingClientRect();
    const x = pageX - OffX;
    const y = pageY - OffY;
    target.touchBegin(pointerId, x, y);
  }

  function touchmove(event) {
    const {
      pageX,
      pageY,
      currentTarget
    } = event;

    if (!currentTarget) {
      return;
    }

    const {
      pointerId
    } = event;
    const target = touchTargets[pointerId];

    if (!target) {
      return;
    }

    const {
      x: OffX,
      y: OffY
    } = currentTarget.getBoundingClientRect();
    const x = pageX - OffX;
    const y = pageY - OffY;
    target.touchMove(pointerId, x, y);
  }

  function touchup(event) {
    const {
      pointerId
    } = event;
    const target = touchTargets[pointerId];

    if (!target) {
      return;
    }

    delete touchTargets[pointerId];
    target.touchEnd(pointerId);
  }

  function pointerdown(event) {
    if (event.pointerType === 'touch') {
      return touchdown(event);
    }
  }

  function pointermove(event) {
    if (event.pointerType === 'touch') {
      return touchmove(event);
    }
  }

  function pointerup(event) {
    if (event.pointerType === 'touch') {
      return touchup(event);
    }
  }

  function touchUpAll() {
    for (let [pointerId, target] of Object.entries(touchTargets)) {
      const k = Number(pointerId);

      if (target) {
        target.mouseEnd(k);
      }

      delete mouseTargets[k];
    }
  }

  function wheel(event) {
    const {
      currentTarget
    } = event;

    if (!currentTarget) {
      return;
    }

    const target = getTarget(event);

    if (!target) {
      return;
    }

    const {
      deltaMode,
      deltaX,
      deltaY,
      pageX,
      pageY
    } = event;
    const {
      x: OffX,
      y: OffY
    } = currentTarget.getBoundingClientRect();
    const x = pageX - OffX;
    const y = pageY - OffY;
    target.wheel(deltaMode, deltaX, deltaY, x, y);
  }

  el.addEventListener('mousedown', updateMouseDown);
  el.addEventListener('mousemove', mousemove);
  el.addEventListener('mouseup', updateMouseDown);
  el.addEventListener('wheel', wheel, true);
  el.addEventListener('pointermove', pointermove);
  el.addEventListener('pointerdown', pointerdown);
  el.addEventListener('pointerup', pointerup);
  el.addEventListener('pointercancel', pointerup);
  el.addEventListener('touchmove', e => e.preventDefault());
  return [() => {
    observer.observe(el);
    document.addEventListener('mouseleave', mouseUpAll);
  }, () => {
    observer.unobserve(el);
    document.removeEventListener('mouseleave', mouseUpAll);
    mouseUpAll();
    touchUpAll();
  }];
}

class MovableArea extends HTMLElement {
  constructor() {
    super();

    _defineProperty(this, "__children", new Set());

    _defineProperty(this, "__connect", void 0);

    _defineProperty(this, "__disconnect", void 0);

    [this.__connect, this.__disconnect] = init(this, this.__children);
  }

  register(o) {
    const children = this.__children;
    children.add(o);
    const {
      width,
      height
    } = this.getBoundingClientRect();
    o.resize(width, height);
    return () => {
      children.delete(o);
    };
  }

  connectedCallback() {
    this.__connect();
  }

  disconnectedCallback() {
    this.__disconnect();
  }

  adoptedCallback() {}

}

function regression(t, v) {
  const tb = t.reduce((a, b) => a + b) / t.length;
  const vb = v.reduce((a, b) => a + b) / v.length;
  return t.map((_, i) => (t[i] - tb) * (v[i] - vb)).reduce((a, b) => a + b) / t.map(a => (a - tb) ** 2).reduce((a, b) => a + b);
}

function getInertiaFn(frame) {
  const time = performance.now() / 1000;
  let last = time;

  for (let i = 0; i < frame.length; i++) {
    const [,, that] = frame[i];

    if (last - that > 0.05) {
      frame.length = i;
      break;
    }

    last = that;
  }

  if (frame.length < 3) {
    return;
  }

  const times = frame.map(f => f[2]);
  const xb = regression(times, frame.map(f => f[0]));
  const yb = regression(times, frame.map(f => f[1]));
  const xs = Math.sign(xb);
  const ys = Math.sign(yb);
  const xv = Math.abs(xb);
  const yv = Math.abs(yb);
  const v = (xv ** 2 + yv ** 2) ** 0.5;
  const xp = xv / v;
  const yp = yv / v;
  const a = 1000;
  const [[xf, yf, b]] = frame;

  function calc() {
    const t = performance.now() / 1000;
    const tt = t - b;
    let nv = v - tt * a;

    if (nv <= 0) {
      const x = xf + xs * xv * v / a / 2;
      const y = yf + ys * yv * v / a / 2;
      return [x, y, true];
    }

    const x = xf + xs * (xv + xv - a * xp * tt) * tt / 2;
    const y = yf + ys * (yv + yv - a * yp * tt) * tt / 2;
    return [x, y, false];
  }

  return calc;
}

function getLength(x1, x2, y1, y2) {
  return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5;
}

function calc(v, area, target) {
  const differential = area - target;
  const min = Math.min(differential, 0);
  const max = Math.max(differential, 0);
  return Math.min(Math.max(min, v), max);
}

function init$1(el, targets) {
  const shadow = el.attachShadow({
    mode: 'closed'
  });
  shadow.appendChild(document.createElement('style')).textContent = ':host{display: block;position: absolute;transform-origin: 0 0;}';
  shadow.appendChild(document.createElement('slot'));
  let needAdaptively = false;

  function adaptively() {
    needAdaptively = true;
    const [aWidth, aHeight] = areaSize;

    if (!aWidth || !aHeight) {
      return;
    }

    needAdaptively = false;
    zoom = Math.min(aWidth / el.clientWidth, aHeight / el.clientHeight);
    x = (aWidth - el.clientWidth * zoom) / 2;
    y = (aHeight - el.clientHeight * zoom) / 2;
    el.style.transform = `translate(${x}px,${y}px)scale(${zoom})`;
  }

  const touches = {};
  let areaSize = [0, 0];
  let x = 0;
  let y = 0;
  let zoom = 1;

  function getZoom(newZoom) {
    const elWidth = el.clientWidth;
    const elHeight = el.clientHeight;
    const [areaWidth, areaHeight] = areaSize;

    if (!areaWidth || !areaHeight) {
      return newZoom;
    }

    const w = areaWidth / elWidth;
    const h = areaHeight / elHeight;
    const max = Math.max(w, h, 5);
    const min = Math.min(w, h, 0.5);
    return Math.max(min, Math.min(newZoom, max));
  }

  let scaleType = '';
  let scaleInfo;

  function scaleBegin(x1, y1, x2, y2) {
    const l = getLength(x1, x2, y1, y2);
    const z = zoom;

    if (!l || !z) {
      scaleInfo = undefined;
      return;
    }

    scaleInfo = {
      x: (x - (x1 + x2) / 2) / z,
      y: (y - (y1 + y2) / 2) / z,
      s: z / l
    };
  }

  function scaleMove(x1, y1, x2, y2) {
    if (!scaleInfo) {
      return;
    }

    const l = getLength(x1, x2, y1, y2);

    if (!l) {
      return;
    }

    const {
      s,
      x: ox,
      y: oy
    } = scaleInfo;
    const newZoom = getZoom(l * s);
    zoom = newZoom;
    x = ox * newZoom + (x1 + x2) / 2;
    y = oy * newZoom + (y1 + y2) / 2;
    update();
  }

  function scaleStop() {
    scaleType = '';
    scaleInfo = undefined;
  }

  let moveType = '';
  let moveInfo;
  let moveAnimation = 0;

  function moveBegin(pageX, pageY) {
    cancelAnimationFrame(moveAnimation);
    moveInfo = undefined;
    moveInfo = {
      x,
      y,
      frame: [[x, y, performance.now() / 1000]],
      bx: pageX,
      by: pageY
    };
  }

  function moveMove(pageX, pageY) {
    if (!moveInfo) {
      return;
    }

    const {
      x: ox,
      y: oy,
      bx,
      by
    } = moveInfo;
    x = ox + pageX - bx;
    y = oy + pageY - by;
    moveInfo.frame = [[x, y, performance.now() / 1000], ...moveInfo.frame].slice(0, 20);
    update();
  }

  function moveEnd() {
    if (!moveInfo) {
      return;
    }

    moveType = '';
    const {
      frame
    } = moveInfo;
    moveInfo = undefined;

    if (!el.inertia) {
      return;
    }

    const calc = getInertiaFn(frame);

    if (!calc) {
      return;
    }

    const run = () => {
      let stop;
      [x, y, stop] = calc();
      update();

      if (!el.inertia) {
        return;
      }

      if (stop) {
        return;
      }

      moveAnimation = requestAnimationFrame(run);
    };

    run();
  }

  function moveStop() {
    cancelAnimationFrame(moveAnimation);
    moveType = '';
    moveInfo = undefined;
  }

  const movableAreaMember = {
    resize(width, height) {
      areaSize = [width, height];

      if (needAdaptively) {
        adaptively();
      }
    },

    match(e) {
      if (!e.length) {
        return el.global || false;
      }

      if (e[0] !== el) {
        return false;
      }

      if (!targets.size) {
        return true;
      }

      const list = new Set(e);

      for (const t of targets) {
        if (list.has(t)) {
          return true;
        }
      }

      return null;
    },

    touchMove(id, x, y) {
      touches[id] = [x, y];
      const touchList = Object.values(touches);

      if (moveType === 'touch') {
        if (touchList.length !== 1) {
          return moveStop();
        }

        const [t1] = touchList;
        moveMove(t1[0], t1[1]);
      }

      if (scaleType !== 'touch') {
        return;
      }

      if (!el.touchScalable || touchList.length !== 2) {
        return scaleStop();
      }

      const [t1, t2] = touchList;
      scaleMove(t1[0], t1[1], t2[0], t2[1]);
    },

    touchBegin(id, x, y) {
      touches[id] = [x, y];
      const touchList = Object.values(touches);

      if (touchList.length === 1) {
        if (!moveType && el.touchMovable) {
          scaleStop();
          moveType = 'touch';
          moveBegin(x, y);
        }
      } else if (touchList.length === 2) {
        if (!scaleType && el.touchScalable) {
          moveStop();
          scaleType = 'touch';
          const [t1, t2] = touchList;
          scaleBegin(t1[0], t1[1], t2[0], t2[1]);
        }
      }
    },

    touchEnd(id) {
      delete touches[id];

      if (scaleType === 'touch') {
        scaleStop();
      }

      if (moveType === 'touch') {
        moveEnd();
      }
    },

    mouseMove(x, y) {
      if (moveType !== 'leftMouse' && moveType !== 'roller') {
        return;
      }

      moveMove(x, y);
    },

    mouseBegin(x, y, key) {
      if (moveType) {
        return;
      }

      let type = '';

      if (key === 0) {
        if (!el.leftMouseMovable) {
          return;
        }

        type = 'leftMouse';
      } else if (key === 2) {
        if (!el.rollerMovable) {
          return;
        }

        type = 'roller';
      }

      scaleStop();
      moveType = type;
      moveBegin(x, y);
    },

    mouseEnd(key) {
      if (!moveType) {
        return;
      }

      const type = key === 0 ? 'leftMouse' : key === 2 ? 'roller' : '';

      if (moveType !== type) {
        return;
      }

      moveEnd();
    },

    wheel(mode, dx, dy, px, py) {
      if (!el.rollerScalable) {
        return;
      }

      const v = Math.abs(dx) > Math.abs(dy) ? dx : dy;

      if (v === 0) {
        return;
      }

      scaleStop();
      const abs = Math.abs(v);
      let base = 0;

      if (v > 0) {
        switch (mode) {
          case 0:
            base = 1.01;
            break;

          case 1:
            base = 1.25;
            break;

          case 2:
            base = 1.6;
            break;
        }
      } else {
        switch (mode) {
          case 0:
            base = 1 / 1.01;
            break;

          case 1:
            base = 1 / 1.25;
            break;

          case 2:
            base = 1 / 1.6;
            break;
        }
      }

      const oldZoom = zoom;
      const newZoom = getZoom(base ** abs * oldZoom);
      zoom = newZoom;
      x = (x - px) / oldZoom * newZoom + px;
      y = (y - py) / oldZoom * newZoom + py;
      update();
    }

  };

  let unregister = () => {};

  function update() {
    const [areaWidth, areaHeight] = areaSize;

    if (!areaWidth || !areaHeight) {
      el.style.transform = `translate(${x}px,${y}px)scale(${zoom})`;
    }

    x = calc(x, areaWidth, el.clientWidth * zoom);
    y = calc(y, areaHeight, el.clientHeight * zoom);
    el.style.transform = `translate(${x}px,${y}px)scale(${zoom})`;
  }

  return [() => {
    requestAnimationFrame(adaptively);
    let parent = el.parentNode;

    while (parent) {
      if (parent instanceof MovableArea) {
        break;
      }

      parent = parent.parentNode;
    }

    if (parent) {
      unregister = parent.register(movableAreaMember);
    }
  }, () => {
    unregister();
  }, adaptively];
}

function setBoolAttr(el, name, value) {
  if (!value && value !== '') {
    el.removeAttribute(name);
    return;
  }

  el.setAttribute(name, value === true ? '' : value);
}

class MovableBody extends HTMLElement {
  get inertia() {
    return this.getAttribute('inertia') !== null;
  }

  set inertia(v) {
    setBoolAttr(this, 'inertia', v);
  }

  get touchMovable() {
    return this.getAttribute('touch-movable') !== null;
  }

  set touchMovable(v) {
    setBoolAttr(this, 'touch-movable', v);
  }

  get touchScalable() {
    return this.getAttribute('touch-scalable') !== null;
  }

  set touchScalable(v) {
    setBoolAttr(this, 'touch-scalable', v);
  }

  get rollerMovable() {
    return this.getAttribute('roller-movable') !== null;
  }

  set rollerMovable(v) {
    setBoolAttr(this, 'roller-movable', v);
  }

  get rollerScalable() {
    return this.getAttribute('roller-scalable') !== null;
  }

  set rollerScalable(v) {
    setBoolAttr(this, 'roller-scalable', v);
  }

  get leftMouseMovable() {
    return this.getAttribute('left-mouse-movable') !== null;
  }

  set leftMouseMovable(v) {
    setBoolAttr(this, 'left-mouse-movable', v);
  }

  get autoAdaptively() {
    return this.getAttribute('adaptively') !== null;
  }

  set autoAdaptively(v) {
    setBoolAttr(this, 'adaptively', v);
  }

  get global() {
    return this.getAttribute('global') !== null;
  }

  set global(v) {
    setBoolAttr(this, 'global', v);
  }

  constructor() {
    super();

    _defineProperty(this, "__targets", new Set());

    _defineProperty(this, "__connect", void 0);

    _defineProperty(this, "__disconnect", void 0);

    _defineProperty(this, "__adaptively", void 0);

    [this.__connect, this.__disconnect, this.__adaptively] = init$1(this, this.__targets);
  }

  register(o) {
    const targets = this.__targets;
    targets.add(o);
    return () => {
      targets.delete(o);
    };
  }

  adaptively() {
    this.__adaptively();
  }

  connectedCallback() {
    this.__connect();
  }

  disconnectedCallback() {
    this.__disconnect();
  }

  adoptedCallback() {}

}

class MovableTarget extends HTMLElement {
  constructor() {
    super();

    _defineProperty(this, "__disconnect", void 0);

    _defineProperty(this, "__connect", void 0);

    let unregister = () => {};

    this.__connect = () => {
      let parent = this.parentNode;

      while (parent) {
        if (parent instanceof MovableBody) {
          break;
        }

        parent = parent.parentNode;
      }

      if (parent) {
        unregister = parent.register(this);
      }
    };

    this.__disconnect = () => {
      unregister();
    };
  }

  connectedCallback() {
    this.__connect();
  }

  disconnectedCallback() {
    this.__disconnect();
  }

}

function home(go) {
  const root = document.createElement('movable-area');
  const body = root.appendChild(document.createElement('movable-body'));
  body.rollerMovable = true;
  body.rollerScalable = true;
  body.inertia = true;
  body.autoAdaptively = true;
  body.global = true;
  body.touchMovable = true;
  body.touchScalable = true;
  body.leftMouseMovable = true;
  root.className = 'page';
  const form = body.appendChild(document.createElement('form'));
  const btn1 = form.appendChild(document.createElement('button'));
  const btn2 = form.appendChild(document.createElement('button'));
  const btn3 = form.appendChild(document.createElement('button'));
  btn1.type = 'button';
  btn2.type = 'button';
  btn3.type = 'button';
  btn1.addEventListener('click', () => go('9/9/10'));
  btn2.addEventListener('click', () => go('16/16/40'));
  btn3.addEventListener('click', () => go('30/16/99'));
  btn1.appendChild(document.createTextNode('初级'));
  btn2.appendChild(document.createTextNode('中级'));
  btn3.appendChild(document.createTextNode('高级'));
  const btn = form.appendChild(document.createElement('button'));
  btn.appendChild(document.createTextNode('↓↓↓自定义↓↓↓'));
  const inputs = form.appendChild(document.createElement('div'));
  const width = inputs.appendChild(document.createElement('input'));
  const height = inputs.appendChild(document.createElement('input'));
  const length = inputs.appendChild(document.createElement('input'));
  width.type = 'number';
  width.required = true;
  width.placeholder = '宽';
  height.type = 'number';
  height.required = true;
  height.placeholder = '高';
  length.type = 'number';
  length.required = true;
  length.placeholder = '雷';
  form.addEventListener('submit', e => {
    e.preventDefault();
    go(`${width.value}/${height.value}/${length.value}`);
  });
  let destroy = false;
  return {
    root,

    destroy() {
      if (destroy) {
        return;
      }

      destroy = true;
    }

  };
}

function create(width, height, length) {
  width = Math.max(5, Math.min(Math.floor(width || 0) || 0, 30));
  height = Math.max(5, Math.min(Math.floor(height || 0) || 0, 30));
  length = Math.max(1, Math.min(Math.floor(length || 0) || 0, Math.floor(width * height / 2)));
  const cells = [];

  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      cells[y * width + x] = {
        flag: [false],
        sign: 0,
        show: false,
        x,
        y
      };
    }
  }

  return {
    width,
    height,
    length,
    ended: false,
    remainder: width * height - length,
    fail: false,
    inited: false,
    cells
  };
}

function init$2(demine, x, y) {
  if (demine.inited) {
    return;
  }

  demine.inited = true;
  const {
    width,
    height,
    cells
  } = demine;
  let {
    length
  } = demine;
  const indexes = new Array(width * height).fill(0).map((_, i) => i);

  if (x >= 0 && y >= 0) {
    indexes.splice(y * width + x, 1);
  }

  while (length--) {
    const [index] = indexes.splice(Math.floor(Math.random() * indexes.length), 1);
    const x = index % width;
    const y = Math.floor(index / width);
    cells[y * demine.width + x].sign = -1;
    const update = [[x - 1, y - 1], [x - 1, y], [x - 1, y + 1], [x, y - 1], [x, y + 1], [x + 1, y - 1], [x + 1, y], [x + 1, y + 1]];

    for (const [x, y] of update) {
      if (x < 0 || x >= width || y < 0 || y >= height) {
        continue;
      }

      const item = cells[y * demine.width + x];

      if (item.sign !== -1) {
        item.sign++;
      }
    }
  }
}

function getNumber(val, max) {
  if (val !== Math.floor(val)) {
    return -1;
  }

  if (val < 0) {
    return -1;
  }

  if (val >= max) {
    return -1;
  }

  return val;
}

function shown(demine, x, y) {
  x = getNumber(x, demine.width);

  if (x < 0) {
    return false;
  }

  y = getNumber(y, demine.height);

  if (y < 0) {
    return false;
  }

  const data = demine.cells[y * demine.width + x];

  if (!data) {
    return false;
  }

  return data.show;
}

function isFlag(demine, x, y, z) {
  x = getNumber(x, demine.width);

  if (x < 0) {
    return false;
  }

  y = getNumber(y, demine.height);

  if (y < 0) {
    return false;
  }

  const cell = demine.cells[y * demine.width + x];

  if (!cell) {
    return false;
  }

  if (!z || z === Infinity) {
    z = 0;
  }

  z = Math.floor(z);

  if (!z) {
    z = 0;
  }

  return cell.flag[z] || false;
}

function clickAround(demine, x, y) {
  return [...clickItem(demine, x - 1, y - 1), ...clickItem(demine, x - 1, y), ...clickItem(demine, x - 1, y + 1), ...clickItem(demine, x, y - 1), ...clickItem(demine, x, y + 1), ...clickItem(demine, x + 1, y - 1), ...clickItem(demine, x + 1, y), ...clickItem(demine, x + 1, y + 1)];
}

function clickItem(demine, x, y) {
  if (demine.ended) {
    return [];
  }

  if (y < 0) {
    return [];
  }

  if (x < 0) {
    return [];
  }

  if (y >= demine.height) {
    return [];
  }

  if (x >= demine.width) {
    return [];
  }

  const cell = demine.cells[y * demine.width + x];

  if (!cell) {
    return [];
  }

  if (cell.show) {
    return [];
  }

  if (cell.flag[0]) {
    return [];
  }

  cell.show = true;
  const changed = {
    x,
    y,
    flag: cell.flag,
    sign: cell.sign,
    show: true
  };

  if (cell.sign === -1) {
    demine.ended = true;
    demine.fail = true;
    return [changed];
  }

  demine.remainder--;

  if (demine.remainder <= 0) {
    demine.ended = true;
  }

  if (cell.sign !== 0) {
    return [changed];
  }

  return [changed, ...clickAround(demine, x, y)];
}

function click(demine, x, y) {
  if (demine.ended) {
    return [];
  }

  x = getNumber(x, demine.width);

  if (x < 0) {
    return [];
  }

  y = getNumber(y, demine.height);

  if (y < 0) {
    return [];
  }

  init$2(demine, x, y);
  return clickItem(demine, x, y);
}

function setFlag(demine, x, y, z = 0, v) {
  if (demine.ended) {
    return [];
  }

  x = getNumber(x, demine.width);

  if (x < 0) {
    return [];
  }

  y = getNumber(y, demine.height);

  if (y < 0) {
    return [];
  }

  const data = demine.cells[y * demine.width + x];

  if (!data) {
    return [];
  }

  if (data.show) {
    return [];
  }

  if (!z || z === 1 / 0) {
    z = 0;
  }

  z = Math.floor(z);

  if (!z) {
    z = 0;
  }

  data.flag[z] = v || false;
  return [{
    x,
    y,
    flag: data.flag,
    sign: data.sign,
    show: false
  }];
}

function probe(demine, x, y) {
  if (!demine.inited) {
    return click(demine, x, y);
  }

  if (demine.ended) {
    return [];
  }

  x = getNumber(x, demine.width);

  if (x < 0) {
    return [];
  }

  y = getNumber(y, demine.height);

  if (y < 0) {
    return [];
  }

  const cell = demine.cells[y * demine.width + x];

  if (!cell) {
    return [];
  }

  if (!cell.show) {
    return [];
  }

  let flag = 0;
  flag += isFlag(demine, x - 1, y - 1, 0) ? 1 : 0;
  flag += isFlag(demine, x - 1, y, 0) ? 1 : 0;
  flag += isFlag(demine, x - 1, y + 1, 0) ? 1 : 0;
  flag += isFlag(demine, x, y - 1, 0) ? 1 : 0;
  flag += isFlag(demine, x, y + 1) ? 1 : 0;
  flag += isFlag(demine, x + 1, y - 1, 0) ? 1 : 0;
  flag += isFlag(demine, x + 1, y, 0) ? 1 : 0;
  flag += isFlag(demine, x + 1, y + 1, 0) ? 1 : 0;

  if (cell.sign !== flag) {
    return [];
  }

  return clickAround(demine, x, y);
}

function sign(demine, x, y) {
  x = getNumber(x, demine.width);

  if (x < 0) {
    return 0;
  }

  y = getNumber(y, demine.height);

  if (y < 0) {
    return 0;
  }

  return demine.cells[y * demine.width + x].sign;
}

function getNumber$1(val, min, max, def = 10) {
  val = Math.floor(val);

  if (!val) {
    return def;
  }

  if (val < min) {
    return min;
  }

  if (val > max) {
    return max;
  }

  return val;
}

function init$3(width, height, l) {
  const w = getNumber$1(width, 5, 30, 15);
  const h = getNumber$1(height, 5, 30, 10);
  const cellElements = [];
  const root = document.createElement('div');
  const runtime = { ...create(w, h, l),
    root,
    cellElements
  };
  root.className = `demine-area demine-area-row-${h} demine-area-column-${w}`;

  for (let y = 0; y < h; y++) {
    for (let x = 0; x < w; x++) {
      const cell = root.appendChild(document.createElement('div'));
      cellElements[x + y * w] = cell;
    }
  }

  return runtime;
}

function update(runtime, {
  x,
  y,
  sign,
  show,
  flag
}) {
  const {
    width,
    cellElements
  } = runtime;
  const td = cellElements[x + y * width];

  if (!show) {
    td.className = flag[0] ? 'flag' : '';
    return;
  }

  switch (sign) {
    case -1:
      td.className = 'wrong';
      break;

    case 0:
      td.className = 'known';
      break;

    default:
      td.className = 'known';
      td.innerHTML = String(sign);
  }
}

function run(runtime, list) {
  for (const v of list) {
    update(runtime, v);
  }

  if (!runtime.ended) {
    return 0;
  }

  const {
    width,
    height,
    cellElements
  } = runtime;

  for (let i = 0; i < width; i++) {
    for (let j = 0; j < height; j++) {
      const td = cellElements[i + j * width];

      if (td.className === 'wrong') {
        continue;
      }

      const m = sign(runtime, i, j) === -1;
      const f = isFlag(runtime, i, j, 0);

      if (f && m) {
        td.className = 'flag';
      } else if (f && !m) {
        td.className = 'flagNoMine';
      } else if (!f && m) {
        td.className = 'mine';
      }
    }
  }

  if (runtime.fail) {
    return -1;
  }

  return 1;
}

function click$1 (runtime, x, y, f) {
  if (runtime.ended) {
    return 0;
  }

  if (shown(runtime, x, y)) {
    return run(runtime, probe(runtime, x, y));
  }

  if (!f) {
    return run(runtime, click(runtime, x, y));
  }

  if (f === 1) {
    return run(runtime, setFlag(runtime, x, y, 0, !isFlag(runtime, x, y, 0)));
  }

  return 0;
}

function getTimeText(ms) {
  let time = Math.floor(ms / 1000);
  const se = String(time % 60).padStart(2, '0');
  time = Math.floor(time / 60);
  const min = String(time).padStart(2, '0');
  return `${min}:${se}`;
}

class Timer {
  get text() {
    return getTimeText(this.time);
  }

  get time() {
    return this.__time + this.__initialValue;
  }

  get initial() {
    return this.__initialValue;
  }

  set initial(v) {
    this.__initialValue = v;
  }

  get paused() {
    return this.__paused;
  }

  start() {
    if (!this.__paused) {
      return;
    }

    this.__paused = false;
    this.__begin = performance.now() - this.__pauseTime;

    this.__render();
  }

  pause() {
    if (this.__paused) {
      return;
    }

    this.__cancelAnimationFrame();

    this.__paused = true;
    this.__pauseTime = performance.now() - this.__begin;
    this.__time = this.__pauseTime;
  }

  restart() {
    this.__begin = performance.now();

    if (!this.__paused) {
      return;
    }

    this.__paused = false;

    this.__render();
  }

  constructor(initial) {
    _defineProperty(this, "__render", void 0);

    _defineProperty(this, "__cancelAnimationFrame", void 0);

    _defineProperty(this, "__paused", false);

    _defineProperty(this, "__begin", performance.now());

    _defineProperty(this, "__pauseTime", 0);

    _defineProperty(this, "__initialValue", 0);

    _defineProperty(this, "__time", 0);

    _defineProperty(this, "onUpdate", void 0);

    this.__initialValue = initial || 0;

    const render = () => {
      this.__time =  performance.now() - this.__begin;
      t = requestAnimationFrame(render);
      const update = this.onUpdate;

      if (typeof update === 'function') {
        update();
      }
    };

    let t = requestAnimationFrame(render);
    this.__render = render;

    this.__cancelAnimationFrame = () => {
      cancelAnimationFrame(t);
    };
  }

}

function createResult(go, result, timer) {
  const div = document.createElement('div');
  div.className = 'result';
  const close = div.appendChild(document.createElement('btn'));
  close.className = 'close';
  close.addEventListener('click', () => div.remove());
  const title = div.appendChild(document.createElement('h1'));
  title.appendChild(document.createTextNode(result ? '你赢了' : '你输了'));
  title.className = 'title';
  const p = div.appendChild(document.createElement('p'));
  p.appendChild(document.createTextNode('用时：'));
  p.appendChild(document.createTextNode(timer.text));
  const btn = div.appendChild(document.createElement('div'));
  const btn1 = btn.appendChild(document.createElement('button'));
  const btn2 = btn.appendChild(document.createElement('button'));
  btn1.appendChild(document.createTextNode('返回菜单'));
  btn2.appendChild(document.createTextNode('再来一局'));
  btn1.addEventListener('click', () => {
    go(false);
  });
  btn2.addEventListener('click', () => {
    go(true);
  });
  return div;
}

function play(go, width, height, length) {
  const runtime = init$3(width, height, length);
  const main = runtime.root;
  let operate = 0;
  const body = document.createElement('movable-body');
  body.rollerMovable = true;
  body.rollerScalable = true;
  body.inertia = true;
  body.autoAdaptively = true;
  body.global = true;

  function updateOperate(v) {
    operate = v;
    const movable = operate === -1;
    body.touchMovable = movable;
    body.touchScalable = movable;
    body.leftMouseMovable = movable;
    moveBtn.className = operate === -1 ? 'selected' : '';
    dotBtn.className = operate === 0 ? 'selected' : '';
    flagBtn.className = operate === 1 ? 'selected' : '';
  }

  const timer = new Timer();
  timer.pause();
  let ended = false;
  body.appendChild(main);
  main.addEventListener('pointerdown', e => {
    if (ended) {
      return;
    }

    let f = operate;

    if (e.pointerType === 'pen') {
      if (e.button === 0) {
        f = 0;
      } else if (e.button === 1) {
        f = 1;
      } else {
        f = -1;
      }
    } else if (e.pointerType === 'mouse') {
      if (e.button === 0) {
        f = 0;
      } else if (e.button === 2) {
        f = 1;
      } else {
        f = -1;
      }
    }

    if (f === -1) {
      return;
    }

    const {
      x: X,
      y: Y,
      width: w,
      height: h
    } = e.currentTarget.getBoundingClientRect();
    const x = Math.floor((e.pageX - X) / w * runtime.width);
    const y = Math.floor((e.pageY - Y) / h * runtime.height);
    e.preventDefault();
    timer.start();
    const r = click$1(runtime, x, y, f);

    if (!r) {
      return;
    }

    ended = true;
    timer.pause();
    area.appendChild(createResult(e => {
      if (!e) {
        go();
        return;
      }

      go(`${width}/${height}/${length}`);
    }, r === 1, timer));
  });
  const root = document.createElement('div');
  root.className = 'page main';
  const header = root.appendChild(document.createElement('header'));
  const backBtn = header.appendChild(document.createElement('button'));
  const timeShow = header.appendChild(document.createElement('div'));
  const dotBtn = header.appendChild(document.createElement('button'));
  const flagBtn = header.appendChild(document.createElement('button'));
  const moveBtn = header.appendChild(document.createElement('button'));
  timeShow.innerText = '00:00';

  timer.onUpdate = () => timeShow.innerText = timer.text;

  dotBtn.appendChild(document.createTextNode('点'));
  flagBtn.appendChild(document.createTextNode('旗'));
  moveBtn.appendChild(document.createTextNode('移'));
  backBtn.appendChild(document.createTextNode('返回'));
  backBtn.addEventListener('click', () => go());
  dotBtn.addEventListener('click', () => updateOperate(0));
  flagBtn.addEventListener('click', () => updateOperate(1));
  moveBtn.addEventListener('click', () => updateOperate(-1));
  const area = root.appendChild(document.createElement('movable-area'));
  area.appendChild(body);
  let destroy = false;
  updateOperate(0);
  return {
    root,

    destroy() {
      if (destroy) {
        return;
      }

      destroy = true;
      timer.pause();
    }

  };
}

document.addEventListener('contextmenu', e => e.preventDefault());
customElements.define('movable-area', MovableArea);
customElements.define('movable-body', MovableBody);
customElements.define('movable-target', MovableTarget);

function getBaseURI() {
  const base = document.querySelector('base');

  if (!base) {
    return '';
  }

  return base.href.replace(/^[a-z0-1-_]+:\/\/[^/]+\//, '/').replace(/\/[^/]*$/, '');
}

const baseURI = getBaseURI();
const regex = /^\/?(\d+)\/(\d+)\/(\d+)$/;

function go(path = '', b) {
  const v = regex.exec(path);

  if (v) {
    const [, w, h, l] = v;
    switchPage(play(go, Number(w), Number(h), Number(l)));
    path = `/${w}/${h}/${l}`;
  } else {
    path = '/';
    switchPage(home(go));
  }

  const pathname = baseURI + path;

  if (b && pathname === location.pathname) {
    return;
  }

  history.pushState({}, '', baseURI + path);
}

let page = null;

function switchPage(p) {
  if (page) {
    page.root.remove();
    page.destroy();
  }

  page = p;
  document.body.appendChild(p.root);
}

go(location.pathname.substring(baseURI.length), true);
